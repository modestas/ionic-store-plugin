package com.tieto;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.Context;
import android.util.Log;

public class IonicStorePlugin extends CordovaPlugin {

    public static final String TAG = "IonicStorePlugin";

    public IonicStorePlugin() { }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArray of arguments for the plugin.
     * @param callbackContext   The callback context used when calling back into JavaScript.
     * @return                  True when the action was valid, false otherwise.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("hello")) {
            Log.d(TAG, "hi, " + args.getString(0) + "!");
        } else {
            return false;
        }

        callbackContext.success();
        return true;
    }
}
